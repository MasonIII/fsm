package edu.berkeley.cs160.ryanma.fsm;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.content.Intent;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.Card;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.Constants;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.DeckOfCardsLauncherIcon;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.util.ParcelableUtil;

import java.io.InputStream;
import java.util.Iterator;
import java.util.Random;

public class ToqActivity extends Activity {


    private final static String PREFS_FILE= "prefs_file";
    private final static String DECK_OF_CARDS_KEY= "deck_of_cards_key";
    private final static String DECK_OF_CARDS_VERSION_KEY= "deck_of_cards_version_key";

    private DeckOfCardsManager mDeckOfCardsManager;
    private RemoteDeckOfCards mRemoteDeckOfCards;
    private RemoteResourceStore mRemoteResourceStore;
    private CardImage[] mCardImages;
    private ToqBroadcastReceiver toqReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toq);
        mDeckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
        toqReceiver = new ToqBroadcastReceiver();
        init();
        setupUI();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (intent.getExtras() != null) {
            Bitmap bitmap = (Bitmap) extras.get("bitmap");
            addSimpleImageCard(bitmap);
            Intent backIntent = new Intent(ToqActivity.this, MainActivity.class);
            ToqActivity.this.startActivity(backIntent);

        }


    }

    // Create some cards with example content
    private RemoteDeckOfCards createDeckOfCards(){

        ListCard listCard= new ListCard();
        SimpleTextCard simpleTextCard= new SimpleTextCard("card0");
        listCard.add(simpleTextCard);

        simpleTextCard = new SimpleTextCard("card1");
        listCard.add(simpleTextCard);
        simpleTextCard = new SimpleTextCard("card2");
        listCard.add(simpleTextCard);
        simpleTextCard = new SimpleTextCard("card3");
        listCard.add(simpleTextCard);
        simpleTextCard = new SimpleTextCard("card4");
        listCard.add(simpleTextCard);
        simpleTextCard = new SimpleTextCard("card5");
        listCard.add(simpleTextCard);
        simpleTextCard = new SimpleTextCard("card6");
        listCard.add(simpleTextCard);
        return new RemoteDeckOfCards(this, listCard);
    }

    // Read an image from assets and return as a bitmap
    private Bitmap getBitmap(String fileName) throws Exception{

        try{
            InputStream is= getAssets().open(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (Exception e){
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }
    }

    private void setupUI() {


        findViewById(R.id.install_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                install();
            }
        });

        findViewById(R.id.uninstall_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uninstall();
            }
        });

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ToqActivity.this,MainActivity.class);
                ToqActivity.this.startActivity(intent);
            }
        });


    }

    private void sendNotification() {
        String[] message0 = new String[2];
        String[] people = new String[6];
        people[1] = "Art Goldberg";
        people[2] = "Jack Weinberg";
        people[3] = "Jackie Goldberg";
        people[4] = "Joan Baez";
        people[5] = "Mario Savio";
        people[0] = "Michael Rossman";
        Random rand = new Random();

        message0[0] = "Please draw:";
        message0[1] = people[rand.nextInt(6)];
        // Create a NotificationTextCard

        NotificationTextCard notificationCard = new NotificationTextCard(System.currentTimeMillis(),
                "Free Speech Movement!", message0);

        // Draw divider between lines of text
        notificationCard.setShowDivider(true);
        // Vibrate to alert user when showing the notification
        notificationCard.setVibeAlert(true);
        // Create a notification with the NotificationTextCard we made
        RemoteToqNotification notification = new RemoteToqNotification(this, notificationCard);

        try {
            // Send the notification
            mDeckOfCardsManager.sendNotification(notification);
            Toast.makeText(this, "Sent Notification", Toast.LENGTH_SHORT).show();
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to send Notification", Toast.LENGTH_SHORT).show();
        }
    }
    /**
     * Installs applet to Toq watch if app is not yet installed
     */
    private void install() {
        boolean isInstalled = true;
        updateDeckOfCardsFromUI();

        try {
            isInstalled = mDeckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error: Can't determine if app is installed", Toast.LENGTH_SHORT).show();
        }

        if (!isInstalled) {
            try {
                mDeckOfCardsManager.installDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
            } catch (RemoteDeckOfCardsException e) {
                e.printStackTrace();
                Toast.makeText(this, "Error: Cannot install application", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "App is already installed!", Toast.LENGTH_SHORT).show();
        }


        try{
            storeDeckOfCards();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

        // Location Interface from Mari's section activity application

        // Acquire a reference to the system Location Manager
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                float[] results = new float[1];
                Location.distanceBetween(latitude, longitude, 37.86965, -122.25914, results);

                if (results[0] < 50) {
                    sendNotification();
                }
                Log.w("FSM", String.valueOf(results[0]));
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };

        // Register the listener with the Location Manager to receive location updates
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 30, locationListener);
        }

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // Get update every 5 seconds
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 30, locationListener);
        }






    }

    private void uninstall() {
        boolean isInstalled = true;

        try {
            isInstalled = mDeckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Error: Can't determine if app is installed", Toast.LENGTH_SHORT).show();
        }

        if (isInstalled) {
            try{
                mDeckOfCardsManager.uninstallDeckOfCards();
            }
            catch (RemoteDeckOfCardsException e){
                Toast.makeText(this, getString(R.string.error_uninstalling_deck_of_cards), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.already_uninstalled), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Adds a deck of cards to the applet
     */
    private void addSimpleImageCard(Bitmap bitmap) {
        ListCard listCard= mRemoteDeckOfCards.getListCard();

        // Create a SimpleTextCard with 1 + the current number of SimpleTextCards
        SimpleTextCard simpleTextCard= (SimpleTextCard)listCard.childAtIndex(6);
        simpleTextCard.setHeaderText("FSM Art");
        mCardImages[6] = new CardImage("card.image.7", bitmap);
        mRemoteResourceStore.addResource(mCardImages[6]);
        simpleTextCard.setCardImage(mRemoteResourceStore, mCardImages[6]);

        simpleTextCard.setReceivingEvents(true);
        simpleTextCard.setShowDivider((true));

        try {
            mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards, mRemoteResourceStore);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to Create SimpleTextCard", Toast.LENGTH_SHORT).show();
        }
    }

    private RemoteDeckOfCards getStoredDeckOfCards() throws Exception{

        if (!isValidDeckOfCards()){
            Log.w(Constants.TAG, "Stored deck of cards not valid for this version of the demo, recreating...");
            return null;
        }

        SharedPreferences prefs= getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        String deckOfCardsStr= prefs.getString(DECK_OF_CARDS_KEY, null);

        if (deckOfCardsStr == null){
            return null;
        }
        else{
            return ParcelableUtil.unmarshall(deckOfCardsStr, RemoteDeckOfCards.CREATOR);
        }

    }

    // Check if the stored deck of cards is valid for this version of the demo
    private boolean isValidDeckOfCards(){

        SharedPreferences prefs= getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        // Return 0 if DECK_OF_CARDS_VERSION_KEY isn't found
        int deckOfCardsVersion= prefs.getInt(DECK_OF_CARDS_VERSION_KEY, 0);

        return deckOfCardsVersion >= Constants.VERSION_CODE;
    }



    private void removeDeckOfCards() {
        ListCard listCard = mRemoteDeckOfCards.getListCard();
        if (listCard.size() == 0) {
            return;
        }

        listCard.remove(0);

        try {
            mDeckOfCardsManager.updateDeckOfCards(mRemoteDeckOfCards);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
            Toast.makeText(this, "Failed to delete Card from ListCard", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Uses SharedPreferences to store the deck of cards
     * This is mainly used to
     */
    private void storeDeckOfCards() throws Exception{
        // Retrieve and hold the contents of PREFS_FILE, or create one when you retrieve an editor (SharedPreferences.edit())
        SharedPreferences prefs = getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        // Create new editor with preferences above
        SharedPreferences.Editor editor = prefs.edit();
        // Store an encoded string of the deck of cards with key DECK_OF_CARDS_KEY
        editor.putString(DECK_OF_CARDS_KEY, ParcelableUtil.marshall(mRemoteDeckOfCards));
        // Store the version code with key DECK_OF_CARDS_VERSION_KEY
        editor.putInt(DECK_OF_CARDS_VERSION_KEY, Constants.VERSION_CODE);
        // Commit these changes
        editor.commit();
    }

    //    Initialise
    private void init(){

        // Create the resource store for icons and images
        mRemoteResourceStore= new RemoteResourceStore();


        mCardImages = new CardImage[7];
        try{
            mCardImages[0]= new CardImage("card.image.1", getBitmap("art_goldberg_toq.png"));
            mCardImages[1]= new CardImage("card.image.2", getBitmap("jack_weinberg_toq.png"));
            mCardImages[2]= new CardImage("card.image.3", getBitmap("jackie_goldberg_toq.png"));
            mCardImages[3]= new CardImage("card.image.4", getBitmap("joan_baez_toq.png"));
            mCardImages[4]= new CardImage("card.image.5", getBitmap("mario_savio_toq.png"));
            mCardImages[5]= new CardImage("card.image.6", getBitmap("michael_rossman_toq.png"));

        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("Can't get picture icon");
            return;
        }

        // Try to retrieve a stored deck of cards
        try {
            // If there is no stored deck of cards or it is unusable, then create new and store
            if ((mRemoteDeckOfCards = getStoredDeckOfCards()) == null){
                mRemoteDeckOfCards = createDeckOfCards();
                storeDeckOfCards();
            }
        }
        catch (Throwable th){
            th.printStackTrace();
            mRemoteDeckOfCards = null; // Reset to force recreate
        }

        // Make sure in usable state
        if (mRemoteDeckOfCards == null){
            mRemoteDeckOfCards = createDeckOfCards();
        }

        // Re-populate the resource store with any card images being used by any of the cards
        for (Iterator<Card> it= mRemoteDeckOfCards.getListCard().iterator(); it.hasNext();){

            String cardImageId= ((SimpleTextCard)it.next()).getCardImageId();

            if ((cardImageId != null) && !mRemoteResourceStore.containsId(cardImageId)){

                if (cardImageId.equals("card.image.1")){
                    mRemoteResourceStore.addResource(mCardImages[0]);
                }
                if (cardImageId.equals("card.image.2")){
                    mRemoteResourceStore.addResource(mCardImages[1]);
                }
                if (cardImageId.equals("card.image.3")){
                    mRemoteResourceStore.addResource(mCardImages[2]);
                }
                if (cardImageId.equals("card.image.4")){
                    mRemoteResourceStore.addResource(mCardImages[3]);
                }
                if (cardImageId.equals("card.image.5")){
                    mRemoteResourceStore.addResource(mCardImages[4]);
                }
                if (cardImageId.equals("card.image.6")){
                    mRemoteResourceStore.addResource(mCardImages[5]);
                }

            }
        }
    }

    private void updateDeckOfCardsFromUI() {
        if (mRemoteDeckOfCards == null) {
            mRemoteDeckOfCards = createDeckOfCards();
        }
        ListCard listCard= mRemoteDeckOfCards.getListCard();
        int currSize = listCard.size();
        // Card #1
        SimpleTextCard simpleTextCard1= (SimpleTextCard)listCard.childAtIndex(0);
        simpleTextCard1.setHeaderText("Art Goldberg");
        simpleTextCard1.setTitleText("Draw: Now");
        simpleTextCard1.setCardImage(mRemoteResourceStore, mCardImages[0]);
        simpleTextCard1.setReceivingEvents(true);
        simpleTextCard1.setShowDivider(true);

        SimpleTextCard simpleTextCard2= (SimpleTextCard)listCard.childAtIndex(1);
        simpleTextCard2.setHeaderText("Jack Weinberg");
        simpleTextCard2.setTitleText("Draw: FSM");
        simpleTextCard2.setCardImage(mRemoteResourceStore, mCardImages[1]);
        simpleTextCard2.setReceivingEvents(true);
        simpleTextCard2.setShowDivider(true);

        SimpleTextCard simpleTextCard3= (SimpleTextCard)listCard.childAtIndex(2);
        simpleTextCard3.setHeaderText("Jackie Goldberg");
        simpleTextCard3.setTitleText("Draw: SLATE");
        simpleTextCard3.setCardImage(mRemoteResourceStore, mCardImages[2]);
        simpleTextCard3.setReceivingEvents(true);
        simpleTextCard3.setShowDivider(true);

        SimpleTextCard simpleTextCard4= (SimpleTextCard)listCard.childAtIndex(3);
        simpleTextCard4.setHeaderText("Joan Baez");
        simpleTextCard4.setTitleText("Draw: A megaphone");
        simpleTextCard4.setCardImage(mRemoteResourceStore, mCardImages[3]);
        simpleTextCard4.setReceivingEvents(true);
        simpleTextCard4.setShowDivider(true);

        SimpleTextCard simpleTextCard5= (SimpleTextCard)listCard.childAtIndex(4);
        simpleTextCard5.setHeaderText("Mario Savio");
        simpleTextCard5.setTitleText("Draw: Your view of Free Speech");
        simpleTextCard5.setCardImage(mRemoteResourceStore, mCardImages[4]);
        simpleTextCard5.setReceivingEvents(true);
        simpleTextCard5.setShowDivider(true);

        SimpleTextCard simpleTextCard6= (SimpleTextCard)listCard.childAtIndex(5);
        simpleTextCard6.setHeaderText("Michael Rossman");
        simpleTextCard6.setTitleText("Draw: 'Free Speech'");
        simpleTextCard6.setCardImage(mRemoteResourceStore, mCardImages[5]);
        simpleTextCard6.setReceivingEvents(true);
        simpleTextCard6.setShowDivider(true);


    }

    /**
     * @see android.app.Activity#onStart()
     */
    protected void onStart(){
        super.onStart();


        // If not connected, try to connect
        if (!mDeckOfCardsManager.isConnected()){
            try{
                mDeckOfCardsManager.connect();
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }
        mDeckOfCardsManager.addDeckOfCardsEventListener(new DeckOfCardsEventListener() {
            @Override
            public void onCardOpen(String s) {
                Intent intent = new Intent(ToqActivity.this, DrawActivity.class);
                intent.setAction("android.intent.action.MAIN");
                intent.addCategory("android.intent.category.LAUNCHER");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                ToqActivity.this.startActivity(intent);
            }

            @Override
            public void onCardVisible(String s) {

            }

            @Override
            public void onCardInvisible(String s) {

            }

            @Override
            public void onCardClosed(String s) {

            }

            @Override
            public void onMenuOptionSelected(String s, String s2) {

            }

            @Override
            public void onMenuOptionSelected(String s, String s2, String s3) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
