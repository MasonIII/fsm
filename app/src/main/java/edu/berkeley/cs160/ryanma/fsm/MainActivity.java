package edu.berkeley.cs160.ryanma.fsm;

import android.widget.Button;
import android.view.View;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;


import java.io.File;


import com.gmail.yuyang226.flickrj.sample.android.FlickrjActivity;

/**
 * Created by ryanma on 10/11/14.
 */
/*
 * Button styles from http://www.dibbus.com/2011/02/gradient-buttons-for-android/
 */
public class MainActivity extends Activity{

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button toqbtn= (Button) findViewById(R.id.toq_btn);
        toqbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, ToqActivity.class);
                MainActivity.this.startActivity(myIntent);
            }
        });

        Button drawbtn= (Button) findViewById(R.id.drawing_btn);
        drawbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, DrawActivity.class);
                MainActivity.this.startActivity(myIntent);
            }
        });

    }
    File fileUri;


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 102) {

            if (resultCode == Activity.RESULT_OK) {
                Uri tmp_fileUri = data.getData();
                String selectedImagePath = getPath(tmp_fileUri);
                fileUri = new File(selectedImagePath);
                Intent intent = new Intent(MainActivity.this,
                        FlickrjActivity.class);
                intent.putExtra("flickImagePath", fileUri.getAbsolutePath());

                startActivity(intent);
            }

        }
    };
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    public void onResume() {
        super.onResume();
    }








}